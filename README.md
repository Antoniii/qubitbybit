> Копенгагенская интерпретация утверждает, что нет объективной реальности, а есть лишь вычисления.

> Корпускулярно-волновой дуализм представляет собой не дуализм структуры объекта, а дуализм модельных способов её описания.

# [Programming Quantum Computers. Code samples](https://oreilly-qc.github.io/)

![](https://qiskit.org/textbook/ch-states/images/bloch.png)

## Sources

* [Программирование квантовых компьютеров. Базовые алгоритмы и примеры кода](https://t.me/arch_progr/1743)
* [QCEngine is a quantum computer simulator](https://oreilly-qc.github.io/docs/build/index.html)
* [JavaScript Introduction](https://oreilly-qc.github.io/docs/build/javascript.html)
* [Учимся квантовому программированию с помощью примеров. Доклад Яндекса](https://habr.com/ru/company/yandex/blog/510054/)
* [Квантовые вычисления и язык Q# для начинающих](https://habr.com/ru/company/microsoft/blog/351622/)
* [Основные сведения о квантовом машинном обучении](https://learn.microsoft.com/ru-ru/azure/quantum/overview-quantum-machine-learning)
* [LightCone](http://lightcone.ru/)

![](https://gitlab.com/Antoniii/qubitbybit/-/raw/main/socks.jpeg)
